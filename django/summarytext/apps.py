from django.apps import AppConfig


class SummarytextConfig(AppConfig):
    name = 'summarytext'
