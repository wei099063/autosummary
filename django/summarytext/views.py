from django.shortcuts import render
from django.http import HttpResponse
from lib.cluster import SummaryTxt
from lib.S2 import SummaryS2
from lib.D1 import SummaryD1
from lib.T2 import SummaryT2
# Create your views here.

def summary_text(request):
	function = request.GET.get('function')
	print(function)
	textarea = request.GET.get('textarea')
	if function == 'a':
		c = SummaryS2()
		try:
			c.txt = textarea
			text = ''.join(c._rank())
		except Exception as e:
			text = ''
			pass
	
	if function == 'b':
		c = SummaryTxt('')
		try:
			text = ''.join(c.summaryTopNtxt(textarea))
		except Exception as e:
			text = ''
			pass
	
	if function == 'c':
		c = SummaryD1()
		try:
			c.txt = textarea
			text = ''.join(c._rank())
		except Exception as e:
			print(e)	
			text = ''
			pass
	
	if function == 'd':
		c = SummaryT2()
		try:
			c.txt = textarea
			text = ''.join(c._rank())

		except Exception as e:
			print(e)
			text = ''
			pass
	fun_list = {'a' : '維度約化與主題地圖方法', 'b' : '語句分群方法', 'c' : '原文相似度方法', 'd' : 'LSA標題相似度方法'}

	if function not in fun_list:
		text = ''
		function = '請選擇方法'
	for i in fun_list:
		if function == i:
			function = fun_list[i]

	context = {'text' : text, 'function' : function, 'textarea' : textarea}
	return render(request, 'summary_text.html', context)
