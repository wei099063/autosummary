from django.db import models
from django.contrib import admin
# Create your models here.
class news(models.Model):
	news_name = models.CharField(max_length = 100)
	news_type = models.CharField(max_length = 20)
	news_content = models.TextField()
	news_date = models.CharField(max_length = 20)
	news_url = models.TextField()
	def __str__(self):
		return self.news_name
@admin.register(news)		
class newsAdmin(admin.ModelAdmin):
	list_display = ('news_name', 'news_type', 'news_date') 