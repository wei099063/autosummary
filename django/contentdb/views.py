from django.shortcuts import render
from django.http import HttpResponse
from .models import news
from lib.crawler import crawler
# Create your views here.
def add_news(request):
	# news.objects.all().delete()
	news_list = news.objects.all()
	obj = crawler()
	obj.parser()
	obj_lenth = len(obj.title)
	news_amount = len(news_list)
	stat = 0  #0 = 更新完成但未增加新聞 1 = 更新完成且有更新新聞
	upgarde = []
	if len(news_list) > 0:
		for i in range(obj_lenth):
			exist = False
			for j in news_list:
				j = str(j)
				if  j == obj.title[i]:
					exist = True
			if exist == False:
				print('create' + obj.title[i] + 'to table')
				news.objects.create(news_name=obj.title[i], news_type=obj.type_name[i], 
									news_content=obj.content[i], news_date=obj.updatetime[i],
									news_url=obj.url[i]
									)
				stat = 1
				upgarde.append(obj.title[i])
			else:
				print(obj.title[i] + 'is exist')
	else:
		for i in range(obj_lenth):
			news.objects.create(news_name=obj.title[i], news_type=obj.type_name[i], 
								news_content=obj.content[i], news_date=obj.updatetime[i], 
								news_url=obj.url[i])

	if stat == 0:
		stat_text = '更新完成但未增加新聞'
	else:
		stat_text = '更新完成且有更新新聞'
	context = {'news_list' : news_list, 'news_amount' : news_amount, 'stat' : stat_text, 'upgarde' : upgarde}
				
	return render(request, 'add.html', context)
def delete_news(request):
	news_list = news.objects.all()
	return render(request, 'delete.html')
