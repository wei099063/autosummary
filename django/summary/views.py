from django.shortcuts import render
from django.http import HttpResponse
from contentdb.models import news
from lib.cluster import SummaryTxt
import random
# Create your views here.

def summary_news(request):
	c = SummaryTxt('')
	try :
		a = request.GET['searchs']
		news_list = news.objects.filter(news_name__contains= a).order_by('-news_date')
	except :
		news_list = news.objects.all().order_by('-news_date')[:5]
		# print(type(news_list))
	news_list_s = []
	for n in news_list:
		news_list_s.append(''.join(c.summaryTopNtxt(n.news_content)[0:3]))
	#-----------------------------主畫面新聞
	d = SummaryTxt('')
	latest_news_list = news.objects.all().order_by('-news_date')[:5]
	latest_news_list_s = []
	for n in latest_news_list:
		latest_news_list_s.append(''.join(d.summaryTopNtxt(n.news_content)[0:3]))
	#-----------------------------最新新聞
	e = SummaryTxt('')
	i = random.randint(0,100)   
	hot_news_list = news.objects.all()[i:i+5]
	hot_news_list_s = []
	for n in hot_news_list:
		hot_news_list_s.append(''.join(e.summaryTopNtxt(n.news_content)[0:3]))
	#-----------------------------熱門新聞
	news_list_conut = zip(news_list, news_list_s)
	latest_news_list_conut = zip(latest_news_list, latest_news_list_s)
	hot_news_list_conut = zip(hot_news_list, hot_news_list_s)

	context = {'news_list' : news_list_conut, 'latest_news_list' : latest_news_list_conut, 'hot_news_list' : hot_news_list_conut}
	return render(request, 'summary_news.html', context)
