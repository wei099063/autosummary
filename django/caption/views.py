from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def caption(request):
	return render(request, 'caption.html')