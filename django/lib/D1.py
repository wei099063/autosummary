import jieba
import numpy as np
#from numpy import linalg as la
from jieba import analyse
#from sklearn.decomposition import PCA
import os
#del sentences[0]

class SummaryD1:
    def __init__(self):
        self.txt=''
        #加載停用詞
        self.stopwrods = {}
    
    def _split_sentences(self,texts): #分句
        splitstr = '!?。！？'
        start = 0
        index = 0  # 每個字符的位置
        sentences = []
        for text in texts:
            if text in splitstr:  # 檢查標點符號下一個字符是否還是標點
                sentences.append(texts[start:index + 1])  # 當前標點符號位置
                start = index + 1  # start標記到下一句的開頭
            index += 1
        if start < len(texts):
            sentences.append(texts[start:])  # 這是為了處理文本末尾沒有標
        self.titlesentence = sentences[0]
        return sentences
    def _keyword(self,texts): #關鍵詞
        tfidf = analyse.extract_tags
        keywords = tfidf(self.txt)
        return(keywords)
    def _split_SandW(self,list): #斷詞斷句
        sentencesset = self._split_sentences(self.txt)
        split_sentences_split_words = []
        stoplist = [line.strip() for line in open('lib/stopwordspath.txt', 'r', encoding='utf8').readlines()]
        self.stopwords = {}.fromkeys(stoplist)
        for sentences in sentencesset:
            seg_list = jieba.cut_for_search(sentences)
            wordsset = []
            for words in seg_list:
                if words not in self.stopwords:
                    num = ['0','1','2','3','4','5','6','7','8','9']
                    for i in num:
                        if i in words:
                            words = ''
                    if words != '' and words != ' ':
                        wordsset.append(words)
            split_sentences_split_words.append(wordsset)
        return split_sentences_split_words
    def _docvactor(self,text,keywordlist):
        wordset = []
        D = []
        seg = jieba.cut_for_search(text)
        for words in seg:
            wordset.append(words)
        for i in keywordlist:
            D.append(float(wordset.count(i)))
        return(D)
    def _statement_vector(self,list): #語句向量
        #關鍵詞計數陣列
        #列(橫)是句子 行(直的)是關鍵字 中間的數值就是關鍵字再句子裡的數量
        sentencesset = self._split_sentences(self.txt)
        split_sentences_split_words = self._split_SandW(sentencesset)
        keywordslist = self._keyword(self.txt)
        X=[]
        for j in range(0, len(keywordslist)):
            X.append([])
            for i in range(0, len(split_sentences_split_words)):
                X[j].append(float(split_sentences_split_words[i].count(keywordslist[j])))
        X_array = np.asarray(X)
        return X_array
    def _Similarity(self,array): #利用X_new計算相似度
        sentencesset = self._split_sentences(self.txt)
        split_sentences_split_words = self._split_SandW(sentencesset)
        X_array = self._statement_vector(split_sentences_split_words)
        keywordslist = self._keyword(self.txt)
        D = self._docvactor(self.txt,keywordslist)
        a,b= np.shape(X_array)
        Similarity = np.zeros([b,2])
        for i in range(0,b):
                    vec1 = D
                    vec2 = X_array[:,i]
                    if np.linalg.norm(vec1)==0 or np.linalg.norm(vec2)==0:
                        Similarity[i][1] = 0
                    else:
                        dist1=float(np.dot(vec1,vec2)/np.linalg.norm(vec1)/np.linalg.norm(vec2))
                        Similarity[i][1] = dist1
                    Similarity[i][0] = i
        return Similarity       
    def _rank(self):
        sentencesset = self._split_sentences(self.txt)
        Similarity = self._Similarity(self.txt)
        for i in range(len(Similarity)):
            if str(Similarity[i][1]) == 'nan':
                Similarity[i][1] = 0
        Similarity_sort = np.argsort(-Similarity[:,1])
        #取topk數 就是總句數的0.35% 壓縮比
        topk = int(0.2*(len(Similarity)-1))
        while topk<1:
            topk =1
        #前K句的位置
        topk_similarity_sort = Similarity_sort[:topk]
        topk_similarity_sort = topk_similarity_sort[topk_similarity_sort.argsort()]
        #放進句子放進list
        topk_sentences = []
        for i in range(topk):
            topk_sentences.append([topk_similarity_sort[i],sentencesset[topk_similarity_sort[i]]])
        topk_sentences_array = np.asarray(topk_sentences)
        summary_title_array = topk_sentences_array[:,1]
        summary_title = []
        for i in range(len(summary_title_array)):
            summary_title.append(summary_title_array[i])
        return summary_title                
if __name__=='__main__':
    Summary()._readall()