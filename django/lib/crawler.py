# -*- coding: utf-8 -*-
"""
Created on Sun Feb 24 23:37:40 2019

@author: yie
"""
from bs4 import BeautifulSoup
import feedparser
import requests
import math
class crawler:
  def __init__(self):
    self.urls = ['http://feeds.feedburner.com/cnaFirstNews',
      'http://feeds.feedburner.com/rsscna/intworld',
      'http://feeds.feedburner.com/rsscna/politics', 'http://feeds.feedburner.com/rsscna/finance', 
      'http://feeds.feedburner.com/rsscna/mainland','http://feeds.feedburner.com/rsscna/social', 
      'http://feeds.feedburner.com/rsscna/technology', 'http://feeds.feedburner.com/rsscna/sport', 
      'http://feeds.feedburner.com/rsscna/stars', 'http://feeds.feedburner.com/rsscna/health', 
      'http://feeds.feedburner.com/rsscna/local'
     ]
    self.title = []
    self.content = []
    self.type_name = []
    self.updatetime = []
    self.url = []
  def parser(self):
    count = 0
    for urls in self.urls:
      rss = feedparser.parse(urls)
      amount = len(rss['entries'])
      print(urls)
      for n in range(amount):
        title = rss['entries'][n]['title']
        self.title.append(title)

        url = requests.get(rss['entries'][n]['link'])
        # print(rss['entries'][n]['link'])
        var = math.ceil(n/amount*100)
        if var % 10 == 0 :
          if count == 0:
            print(var, '%')
            count = 1
          else:
            count = 0
        self.url.append(str(rss['entries'][n]['link']))
        soup = BeautifulSoup(url.text,'lxml')
        content_box = soup.find_all('p')
        content = ''
        for text in content_box:
          content += text.text  
        self.content.append(str(content))
        type_soup = soup.title
        type_text = type_soup.text.lstrip()
        t = type_text.find('|')
        type_ = type_text[t+2 : t+4]
        self.type_name.append(type_)
        try:
          time_soup = soup.find('div', class_='updatetime')
          time = time_soup.find('span').text
          self.updatetime.append(time)  
        except:
          self.updatetime.append('unknown')
          pass
      print('100%')
    return self.title, self.content, self.type_name, self.updatetime, self.url
