from django.shortcuts import render
from contentdb.models import news
from lib.cluster import SummaryTxt
from django.http import HttpResponse

# Create your views here.

def welcome(request):
	return render(request, 'home.html')