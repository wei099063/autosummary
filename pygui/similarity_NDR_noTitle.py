# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 23:49:01 2019

@author: yie09
"""
import jieba
import numpy as np
from numpy import linalg as la
from jieba import analyse


class similarity_NDR_noTitle:
    def __init__(self,n):
        self.ratio = 0
        self.txt = ''
    def _split_sentences(self,texts): #分句
        splitstr = '!?。！？'
        start = 0
        index = 0  # 每個字符的位置p
        sentences = []
        for text in texts:
            if text in splitstr:  # 檢查標點符號下一個字符是否還是標點
                sentences.append(texts[start:index + 1])  # 當前標點符號位置
                start = index + 1  # start標記到下一句的開頭
            index += 1
        if start < len(texts):
            sentences.append(texts[start:])  # 這是為了處理文本末尾沒有標
        del sentences[0]
        return sentences
    def _keyword(self,texts): #關鍵詞
        tfidf = analyse.extract_tags
        keywords = tfidf(self.txt)
        return(keywords)
    def _split_SandW(self,list): #斷詞斷句
        sentencesset = self._split_sentences(self.txt)
        split_sentences_split_words = []
        for sentences in sentencesset:
            seg_list = jieba.cut_for_search(sentences)
            wordsset = []
            for words in seg_list:
                wordsset.append(words)
            split_sentences_split_words.append(wordsset)
        return split_sentences_split_words
    def _statement_vector(self,list): #語句向量
        #關鍵詞計數陣列
        #列(橫)是句子 行(直的)是關鍵字 中間的數值就是關鍵字再句子裡的數量
        sentencesset = self._split_sentences(self.txt)
        split_sentences_split_words = self._split_SandW(sentencesset)
        keywordslist = self._keyword(self.txt)
        X=[]
        for j in range(0, len(keywordslist)):
            X.append([])
            for i in range(0, len(split_sentences_split_words)):
                X[j].append(float(split_sentences_split_words[i].count(keywordslist[j])))
        X_array = np.asarray(X)
        #離差標準化(0~1)
        for i in range(0, len(keywordslist)):
            for j in range(0, len(split_sentences_split_words)):
                if X_array[i][j] > 0:
                    X_array[i][j] = (X_array[i][j]-X_array.min()) / (X_array.max()-X_array.min())
        X_new = np.transpose(X_array)
        return X_new
    def _Similarity(self,array): #利用X_new計算相似度
        sentencesset = self._split_sentences(self.txt)
        split_sentences_split_words = self._split_SandW(sentencesset)
        X_new = self._statement_vector(split_sentences_split_words)
        Similarity = np.zeros([len(X_new),len(X_new)])
        for i in range(0,len(X_new)):
            for j in range(0,len(X_new)):
                if i == j:
                    Similarity[i][j] = 0
                if i < j:
                    vec1 = X_new[i,:]
                    vec2 = X_new[j,:]
                    if np.linalg.norm(vec1)==0 or np.linalg.norm(vec2)==0:
                        Similarity[i][j] = 0
                    else:
                        dist1=float(np.dot(vec1,vec2)/np.linalg.norm(vec1)/np.linalg.norm(vec2))
                        Similarity[i][j] = dist1
                if i > j:
                    Similarity[i][j] = 0
                j += 1
            i += 1
        return Similarity       
    def _map(self,array): #主題地圖
        #只保留1.5倍句數的連結(1.5*Similarity)
        Similarity = self._Similarity(self.txt)
        list_Sim = []                           #Similarity數值存到List
        for i in range(0,len(Similarity)):
            for j in range(0,len(Similarity)):
                list_Sim.append(Similarity[i][j]) 
                j += 1
            i += 1
        list_Sim_1 = [x for x in list_Sim if str(x) != 'nan'] #刪除空值
        list_Sim_1.sort(reverse=True)   #排序，找出要保留的連結域值
        topn = int(1.5 * len(Similarity)) #留下topn句
        Min_Sim = list_Sim_1[topn-1] 
        Similarity[Similarity < Min_Sim] = 0 
        #將點及連結標示出來
        topn_Sim = np.zeros([topn,3])
        k=0
        for i in range(len(Similarity)):
            for j in range(len(Similarity)):
                if Similarity[i][j] > 0:
                    topn_Sim[k][0] = i
                    topn_Sim[k][1] = j
                    topn_Sim[k][2] = Similarity[i][j]
            k += 1
        self.topp = topn_Sim[:,0:2]
        count = np.zeros([len(Similarity),2])
        return count
    def _rank(self):
        #排出最多連結的topk 
        #取topk數 就是總句數的0.35% 壓縮比
        sentencesset = self._split_sentences(self.txt)
        Similarity = self._Similarity(self.txt)
        count = self._map(Similarity)
        topk = int(self.ratio/100.0*len(Similarity))
        for i in range(len(Similarity)):
            count[i][0] = i
            count[i][1] = np.sum ( self.topp == i) 
        count= count[count[:,1].argsort()]
        topk_sentences_num = np.zeros([topk,2])
        k=len(count)-1
        for i in range(topk):
            topk_sentences_num[i][0] = count[k][0]
            topk_sentences_num[i][1] = count[k][1]
            k -= 1
        topk_sentences_sort = topk_sentences_num[topk_sentences_num[:,0].argsort()]
        topk_sentences = []
        for i in range(topk):
            num =int( topk_sentences_sort[i][0] )
            topk_sentences.append(sentencesset[num])
        return topk_sentences