# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 15:08:03 2019

@author: yie09
"""
import tkinter as tk
from similarity_LSA_noTitle import similarity_LSA_noTitle
from similarity_LSA_Title import similarity_LSA_Title
from similarity_DR_Title import similarity_DR_Title
from similarity_TS_KWV import similarity_TS_KWV
from similarity_TS_LSA import similarity_TS_LSA
from similarity_NDR_noTitle import similarity_NDR_noTitle
from cluster import SummaryTxt
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from tkinter import *
import tkinter.font as tkFont


#介面
root = tk.Tk()
root.title('summary')

def openfile():
    tori.delete(0.0, END)
    global txt
    filename = askopenfilename()
    f = open(filename,'r',encoding = 'utf8').read()
    txt = f
    tori.insert(END,f)
    splitstr = '!?。！？'
    start = 0
    index = 0  # 每個字符的位置
    global total
    total = 0 #句數
    global A #字數
    A = 0
    sentences = []
    for text in txt:
        if text in splitstr:  # 檢查標點符號下一個字符是否還是標點
            sentences.append(txt[start:index + 1])  # 當前標點符號位置
            start = index + 1  # start標記到下一句的開頭
            total += 1
        index += 1
        A += 1
    if start < len(txt):
       sentences.append(txt[start:])  # 這是為了處理文本末尾沒有標
    print(total)
    print(A)
def summary():
    tsummary.delete(0.0, END)
    ratio = ratioe.get()
    ratio = int(ratio)
    if comboExample.current() == 0 :
        obj = SummaryTxt('')
        obj.N = int(A/20)
        obj.CLUSTER_THRESHOLD = 5
        obj.TOP_SENTENCES = int(total*ratio/100)
        point = obj.summaryTopNtxt(txt)
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 1 :
        obj = SummaryTxt('')
        obj.N = int(A/20)
        obj.CLUSTER_THRESHOLD = 5
        obj.TOP_SENTENCES = int(total*ratio/100)
        point = obj.summaryTopNtxt(txt)
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 2 :
        obj = SummaryTxt('')
        obj.N = int(A/20)
        obj.CLUSTER_THRESHOLD = 100
        obj.TOP_SENTENCES = int(total*ratio/100)
        point = obj.summaryTopNtxt(txt)
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 3:
        obj = similarity_LSA_Title(0)
        obj.txt = txt
        obj.ratio = ratio
        point = obj._rank()
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 4:
        obj = similarity_LSA_noTitle(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 5:
        obj = similarity_TS_LSA(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 6:
        obj = similarity_TS_KWV(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 7:
        obj = similarity_DR_Title(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')
    if comboExample.current() == 8:
        obj = similarity_NDR_noTitle(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        tsummary.insert(END,point)
        tsummary.insert(END,'\n-----------------------------------------------------------------')
        tsummary.insert(END,'\n')

def summary2():
    fsummary.delete(0.0, END)
    ratio = ratioe.get()
    ratio = int(ratio)
    if comboExample2.current() == 0 :
        obj = SummaryTxt(0)
        obj.N = int(A/20)
        obj.CLUSTER_THRESHOLD = 5
        obj.TOP_SENTENCES = int(total*ratio/100)
        point = obj.summaryTopNtxt(txt)
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 1 :
        obj = SummaryTxt(0)
        obj.N = int(A/20)
        obj.CLUSTER_THRESHOLD = 5
        obj.TOP_SENTENCES = int(total*ratio/100)
        point = obj.summaryTopNtxt(txt)
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 2 :
        obj = SummaryTxt(0)
        obj.N = int(A/20)
        obj.CLUSTER_THRESHOLD = 100
        obj.TOP_SENTENCES = int(total*ratio/100)
        point = obj.summaryTopNtxt(txt)
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 3:
        obj = similarity_LSA_Title(0)
        obj.txt = txt
        obj.ratio = ratio
        point = obj._rank()
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 4:
        obj = similarity_LSA_noTitle(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 5:
        obj = similarity_TS_LSA(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 6:
        obj = similarity_TS_KWV(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 7:
        obj = similarity_DR_Title(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')
    if comboExample2.current() == 8:
        obj = similarity_NDR_noTitle(0)
        obj.ratio = ratio
        obj.txt = txt
        point = obj._rank()
        fsummary.insert(END,point)
        fsummary.insert(END,'\n-----------------------------------------------------------------')
        fsummary.insert(END,'\n')

def print_selection(v):
    l.config(text='壓縮比 ' + v)

ft = tkFont.Font(size=13)

bopen = tk.Button(root, text = "open", command = openfile)
bopen.grid(row=4, column=0,sticky=W)

tori = tk.Text(root, height=31, width=50,font=ft)
tori.grid(row=5, column=0,sticky=W,padx=7, pady=10)

Label1 = tk.Label(root,text='ratio')
Label1.grid(row=2,column=0, sticky=W)

ratioe= tk.Scale(root, label='試拖曳', from_=0, to=100, orient=tk.HORIZONTAL,
             length=350, showvalue=0, command=print_selection)
ratioe.grid(row=2,column=1, sticky=W)

l = tk.Label(root, bg='yellow', width=50, text='壓縮比')
l.grid(row=1,column=1, sticky=W)




Label2 = tk.Label(root,text='摘要方法')
Label2.grid(row=3,column=0, sticky=W)

comboExample = ttk.Combobox(root,values=[
                                    "語句分群", 
                                    "語句分群(含標題)",
                                    "語句分群(沒有clust)",
                                    "LSA標題",
                                    "LSA無標題",
                                    "標題相似度LSA",
                                    "標題相似度_關鍵詞向量",
                                    "降維_標題",
                                    "無降維_無標題",])
comboExample.grid(row=3, column=1,sticky=W)
comboExample.current(3)

comboExample2 = ttk.Combobox(root,values=[
                                    "語句分群", 
                                    "語句分群(含標題)",
                                    "語句分群(沒有clust)",
                                    "LSA標題",
                                    "LSA無標題",
                                    "標題相似度LSA",
                                    "標題相似度_關鍵詞向量",
                                    "降維_標題",
                                    "無降維_無標題",])
comboExample2.grid(row=3, column=2,sticky=W)
comboExample2.current(5)

bsummary = tk.Button(root,text='summary', command = summary)
bsummary.grid(row=4,column=1, sticky=W)

csummary = tk.Button(root,text='summary', command = summary2)
csummary.grid(row=4,column=2, sticky=W)

tsummary =  tk.Text(root, height=31,width=50,font=ft)
tsummary.grid(row=5,column=1, sticky=W,padx=7, pady=10)

fsummary =  tk.Text(root, height=31,width=50,font=ft)
fsummary.grid(row=5,column=2, sticky=W,padx=7, pady=10)

root.mainloop()